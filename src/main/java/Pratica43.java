
import utfpr.ct.dainf.if62c.pratica.Circulo;
import utfpr.ct.dainf.if62c.pratica.Elipse;
import utfpr.ct.dainf.if62c.pratica.Quadrado;
import utfpr.ct.dainf.if62c.pratica.Retangulo;
import utfpr.ct.dainf.if62c.pratica.TrianguloEquilatero;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author Gamer
 */
public class Pratica43 {
    public static void main(String[] args) {
        Retangulo ra = new Retangulo (4.0,6.0);
        Retangulo rb = new Retangulo(5.5,9.5);
        
        Quadrado qa = new Quadrado (4.0);
        Quadrado qb = new Quadrado (5.0);
        
        Elipse ea = new Elipse (4.5,6.3);
        Elipse eb = new Elipse (9.5,6.5);
        
        Circulo ca = new Circulo (8.5);
        Circulo cb = new Circulo (9.5);
        
        TrianguloEquilatero ta = new TrianguloEquilatero (6.0);
        TrianguloEquilatero tb = new TrianguloEquilatero (9.7);
        
        System.out.println(ra.getNome() + "->\tPerímetro primeiro Retângulo: " + ra.getPerimetro() + "\t Área Primeiro Retângulo: " + ra.getArea());
        System.out.println(rb.getNome() + "->\tPerímetro segundo Retângulo: " + rb.getPerimetro() + "\t Área segundo Retângulo: " + rb.getArea());
        System.out.println(qa.getNome() + "->\tPerímetro primeiro quadrado: " + qa.getPerimetro() + "\t Área Primeiro quadrado: " + qa.getArea());
        System.out.println(qb.getNome() + "->\tPerímetro segundo quadrado: " + qb.getPerimetro() + "\t Área segundo quadrado: " + qb.getArea());
        System.out.println(ea.getNome() + "->\tPerímetro primeira elipse: " + ea.getPerimetro() + "\t Área Primeira elipse: " + ea.getArea());
        System.out.println(eb.getNome() + "->\tPerímetro segunda elipse: " + eb.getPerimetro() + "\t Área segunda elipse: " + eb.getArea());
        System.out.println(ca.getNome() + "->\tPerímetro primeiro circulo: " + ca.getPerimetro() + "\t Área Primeiro circulo: " + ca.getArea());
        System.out.println(cb.getNome() + "->\tPerímetro segundo circulo: " + cb.getPerimetro() + "\t Área segundo circulo: " + cb.getArea());
        System.out.println(ta.getNome() + "->\tPerímetro primeiro triangulo: " + ta.getPerimetro() + "\t Área Primeiro triangulo: " + ta.getArea());
        System.out.println(tb.getNome() + "->\tPerímetro segundo triangulo: " + tb.getPerimetro() + "\t Área segundo triangulo: " + tb.getArea());
    }
}
